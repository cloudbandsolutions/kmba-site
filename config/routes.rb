Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root "pages#index"
  get 'about_us',to: 'pages#about_us'
  #get 'products_and_services',to: 'pages#products_and_services'
  #get 'contact_us',to: 'pages#contact_us'
  get 'history',to: 'pages#history'
  get 'history_kdci',to: 'pages#history_kdci'
  get 'vision_and_mission',to: 'pages#vision_and_mission'
  get 'association_objective',to: 'pages#association_objective'
  get 'membership',to: 'pages#membership'
  #get 'clip',to: 'pages#clip'
  #get 'kalinga',to: 'pages#kalinga'
  #get 'other_services',to: 'pages#other_services'
  #get 'governance_manual',to: 'company_documents#governance_manual'
  get 'code_of_conducts_and_ethics',to: 'company_documents#code_of_conducts_and_ethics'
  get 'policies_and_implementing_rules',to: 'company_documents#policies_and_implementing_rules'
  get 'acgs_2014',to: 'company_documents#acgs_2014'  

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
   resources :events_and_features
   resources :galleries
   resources :company_documents
   resources :articles
   resources :partners_and_links
   resources :downloadable_forms
   resources :contact_us
   resources :products_and_services
   resources :memberships
   resources :organizations
   resources :notice_of_agm_disclosures
   resources :notice_of_board_meeting_disclosures
   resources :other_disclosures
   resources :publications
  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end

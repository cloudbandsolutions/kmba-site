ActiveAdmin.register AssociationObjective do

	controller do
 		def permitted_params
 			params.permit!
 	 	end
 	end	

 	index do
 		column :content
 		actions
 	end

	form do |f|	
		f.inputs "Details" do
			f.input :content, input_html: {class: "tinymce"}
		end

		f.actions 
	end

	show do
		attributes_table do
			row :content do |e|
				e.content.html_safe
			end	
		end
	end
end
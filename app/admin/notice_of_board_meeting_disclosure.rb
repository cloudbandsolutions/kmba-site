ActiveAdmin.register NoticeOfBoardMeetingDisclosure do
	controller do
 		def permitted_params
 			params.permit!
 	 	end
 	end	

 	index do
 		column :title
 		actions
 	end

	filter :title

	form do |f|	
		f.inputs "Details" do
			f.input :title
			f.input :published_at
			f.input :description, input_html: {class: "tinymce"}
			f.input :photo, as: :file, hint: image_tag(f.object.photo.url(:large)).html_safe
		end

		f.actions 
	end

	show do
		attributes_table do
			row :title
			row :published_at
			row :description do |e|
				e.description.html_safe
			end
			row :photo do |e|
				image_tag e.photo.url
			end
		end
	end
end

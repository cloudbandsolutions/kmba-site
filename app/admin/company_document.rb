ActiveAdmin.register CompanyDocument do
	
	controller do
 		def permitted_params
 			params.permit!
 	 	end
 	end	

 	index do
 		column :category
 		column :year
 		actions
 	end

 	filter :category
 	filter :year

 	form do |f|	
		f.inputs "Details" do
			f.input :title
			f.input :category, as: :select, collection: CompanyDocument::CATEGORIES
			f.input :year, as: :select, collection: CompanyDocument::YEARS
			#f.input :content, input_html: {class: "tinymce"}
			# f.input :file, as: :file
			f.has_many :company_document_files, allow_destroy: true do |ff|
				ff.input :file, as: :file, hint: ff.object.file.url
				ff.input :published_date
			end
		end

		f.actions 
	end

	show do
		attributes_table do
			row :title
			row :category
			row :year
			# row :content do |e|
			# 	e.content.html_safe
			# end
			# row :file do |e|
			# 	e.file.url
			# end

			div class: 'panel' do
      h3 'Company Documents File'
      div class: 'attributes_table' do
        table do
          tr do
            th class: 'gs' do 
              'Company Document File'
            end
          end
          company_document.company_document_files.each do |cdf|
            tr do
              td class: 'sb' do
                cdf.file.url
              end
            end
            tr do
              td class: 'sb' do
                cdf.published_date
              end
            end
          end
        end
      end
		end
	end
	end
end
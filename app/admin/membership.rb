ActiveAdmin.register Membership do

	controller do
 		def permitted_params
 			params.permit!
 	 	end
 	end	

 	index do
 		column :date_updated
    	column :financial_highlights_fund_balance
    	column :active_member_total
    	column :claims_summary_total
 		actions
 	end

 	filter :barangay

 	form do |f|	
		f.inputs "Details" do
			f.input :date_updated
		    f.input :financial_highlights_assets
		    f.input :financial_highlights_liabilities
		    f.input :financial_highlights_fund_balance
		    f.input :active_member_kcoop
		    f.input :active_member_associates
		    f.input :active_member_total
		    f.input :claims_summary_BLIP
		    f.input :claims_summary_CLIP
		    f.input :claims_summary_HIIP
		    f.input :claims_summary_total
		    f.input :claims_summary_kbente
		    f.input :claims_summary_calamity_assistance
		    f.input :kalinga_amount_of_claims
		end

		f.actions 
	end

	show do
		attributes_table do
			row :date_updated
		    row :financial_highlights_assets
		    row :financial_highlights_liabilities
		    row :financial_highlights_fund_balance
		    row :active_member_kcoop
		    row :active_member_associates
		    row :active_member_total
		    row :claims_summary_BLIP
		    row :claims_summary_CLIP
		    row :claims_summary_HIIP
		    row :claims_summary_total
		    row :claims_summary_kbente
		    row :claims_summary_calamity_assistance
		    row :kalinga_amount_of_claims
		end
	end
end
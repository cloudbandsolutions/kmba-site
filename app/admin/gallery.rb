ActiveAdmin.register Gallery do
	controller do
 		def permitted_params
 			params.permit!
 	 	end
 	end	

 	index do
 		column :name
 		actions
 	end

	filter :name

	form do |f|	
		f.inputs "Details" do
			f.input :name

			f.has_many :gallery_photos, allow_destroy: true do |ff|
				ff.input :photo, as: :file, hint: image_tag(ff.object.photo.url(:thumb)).html_safe
			end
		end

		f.actions 
	end

	show do
		attributes_table do
			row :name
		end

		div class: 'panel' do
      h3 'Gallery Photos'
      div class: 'attributes_table' do
        table do
          tr do
            th class: 'gs' do 
              'Galler Photos'
            end
          end
          gallery.gallery_photos.each do |gp|
            tr do
              td class: 'sb' do
                image_tag gp.photo.url
              end
            end
          end
        end
      end
    end  
	end

end

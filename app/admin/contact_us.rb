ActiveAdmin.register ContactUs do

	controller do
 		def permitted_params
 			params.permit!
 	 	end
 	end	

 	index do
 		column :street
 		column :subdivision
 		column :barangay
 		column :country
 	
 		actions
 	end

 	filter :barangay

 	form do |f|	
		f.inputs "Details" do
			f.input :street
			f.input :subdivision
			f.input :barangay
			f.input :country, as: :select, collection: country_dropdown
			f.input :contact
			f.input :email
			f.input :map_link
		end

		f.actions 
	end

	show do
		attributes_table do
			row :street
			row :subdivision
			row :barangay
			row :country
			row :contact
			row :email
			row :map_link
		end
	end
end
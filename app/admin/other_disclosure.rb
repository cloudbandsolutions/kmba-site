ActiveAdmin.register OtherDisclosure do
	controller do
 		def permitted_params
 			params.permit!
 	 	end
 	end	

 	index do
 		column :title
 		column :published_date
 		actions
 	end

 	filter :title

 	form do |f|	
		f.inputs "Details" do
			f.input :title
			f.input :published_date
			f.input :file, as: :file, hint: f.object.file.url
		end

		f.actions 
	end

	show do
		attributes_table do
			row :title
			row :published_date
			row :file do |e|
				e.file.url
			end
		end
	end
end
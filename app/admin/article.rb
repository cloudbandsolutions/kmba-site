ActiveAdmin.register Article do

	controller do
 		def permitted_params
 			params.permit!
 	 	end
 	end	

 	index do
 		column :file_file_name
 		actions
 	end

	filter :file_file_name

	form do |f|	
		f.inputs "Details" do
				f.input :file, as: :file, hint: f.object.file.url
		end

		f.actions 
	end

	show do
		attributes_table do
			row :file_file_name
			row :file do |e|
				e.file.url
			end	
		end
	end
end
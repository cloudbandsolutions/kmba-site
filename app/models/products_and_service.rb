class ProductsAndService < ActiveRecord::Base

has_attached_file :photo,
styles: { 
  thumb: "80x80>", 
  standard: "150x150>",
  large: "500x500>",
  banner: "750x400>" },
default_url: ""

#validates_attachment_size :photo, :less_than => 5.megabytes  
validates_attachment_content_type :photo, content_type: %w(image/jpeg image/jpg image/png image/bmp)
end

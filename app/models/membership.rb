class Membership < ActiveRecord::Base
	QTR = ["1st Qtr", "2nd Qtr", "3rd Qtr", "4th Qtr"]
	validates :date_updated, presence: true
end

class CompanyDocument < ActiveRecord::Base
	has_many :company_document_files
	CATEGORIES = ["Governance Manual", "Policies and Implementing Rules", "Corporate Social Responsibilities", "ACGS", "ACGR", "Code of Conducts and Ethics", "Minutes of AGM", "Annual Report", "Audited Financial Statement", "Minutes of Board and Special Meeting", "Board Resolution", "Board Committees", "Board of Trustees", "Meetings and Seminar"]
	YEARS = [2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024]
	accepts_nested_attributes_for :company_document_files, reject_if: :all_blank, allow_destroy: true

	validates :category, presence: true, uniqueness: { scope: [:category, :year] }
end

class CompanyDocumentFile < ActiveRecord::Base
	belongs_to :company_document

	has_attached_file :file,
	default_url: ""
	#validates_attachment_content_type :file, content_type: %w(application/pdf application/zip application/msword application/vnd.ms-office application/vnd.ms-excel application/vnd.openxmlformats-officedocument.spreadsheetml.sheet)
	do_not_validate_attachment_file_type :file
end

class Gallery < ActiveRecord::Base
	has_many :gallery_photos
	accepts_nested_attributes_for :gallery_photos, reject_if: :all_blank, allow_destroy: true

	validates :name, presence: true
end

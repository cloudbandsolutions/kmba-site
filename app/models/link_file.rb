class LinkFile < ActiveRecord::Base
	has_attached_file :file,
	default_url: ""
	validates_attachment_content_type :file, content_type: %w(image/jpeg image/jpg image/png image/bmp application/pdf application/zip application/msword application/vnd.ms-office application/vnd.ms-excel application/vnd.openxmlformats-officedocument.spreadsheetml.sheet)

	validates :file_file_name, presence: true, uniqueness: { scope: [:file_file_name] }
end

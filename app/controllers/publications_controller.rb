class PublicationsController < ApplicationController
  def index
    @publications = Publication.all.order("published_at DESC")
  end
end
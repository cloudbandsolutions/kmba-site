class CompanyDocumentsController < ApplicationController

  def index
    @company_documents = CompanyDocument.all
  end

  def show
   	@company_document = CompanyDocument.find(params[:id])
  end
end
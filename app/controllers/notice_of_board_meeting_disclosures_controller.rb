class NoticeOfBoardMeetingDisclosuresController < ApplicationController

  def index
    @notice_of_board_meeting_disclosures = NoticeOfBoardMeetingDisclosure.all
    @notice_of_board_meeting_disclosures = @notice_of_board_meeting_disclosures.page(params[:page]).per(5).order("published_at DESC")
  end

  def new
   	@notice_of_board_meeting_disclosure = NoticeOfBoardMeetingDisclosure.new
  end

  def show
   	@notice_of_board_meeting_disclosure = NoticeOfBoardMeetingDisclosure.find(params[:id])
   	@notice_of_board_meeting_disclosures = NoticeOfBoardMeetingDisclosure.where("id != ?", @notice_of_board_meeting_disclosure.id).order("published_at DESC")
  end

end
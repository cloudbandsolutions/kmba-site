class EventsAndFeaturesController < ApplicationController

  def index
    @events_and_features = EventsAndFeature.all
    @events_and_features = @events_and_features.page(params[:page]).per(5).order("published_at DESC")
  end

  def new
   	@events_and_feature = EventsAndFeature.new
  end

  def show
   	@events_and_feature = EventsAndFeature.find(params[:id])
   	@events_and_features = EventsAndFeature.where("id != ?", @events_and_feature.id).order("published_at DESC")
  end

end
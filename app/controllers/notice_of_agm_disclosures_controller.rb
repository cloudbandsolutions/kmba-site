class NoticeOfAgmDisclosuresController < ApplicationController

  def index
    @notice_of_agm_disclosures = NoticeOfAgmDisclosure.all
    @notice_of_agm_disclosures = @notice_of_agm_disclosures.page(params[:page]).per(5).order("published_at DESC")
  end

  def new
   	@notice_of_agm_disclosure = NoticeOfAgmDisclosure.new
  end

  def show
   	@notice_of_agm_disclosure = NoticeOfAgmDisclosure.find(params[:id])
   	@notice_of_agm_disclosures = NoticeOfAgmDisclosure.where("id != ?", @notice_of_agm_disclosure.id).order("published_at DESC")
  end

end
class OtherDisclosuresController < ApplicationController
  def index
    @other_disclosures = OtherDisclosure.all
    @other_disclosures = @other_disclosures.page(params[:page]).per(15).order("published_date DESC")
  end
end
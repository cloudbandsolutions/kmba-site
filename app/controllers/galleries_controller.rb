class GalleriesController < ApplicationController

  def index
    @galleries = Gallery.all
    @galleries = @galleries.page(params[:page]).per(5).order("created_at DESC")
  end
 
  def show
   	@gallery = Gallery.find(params[:id])
  end


  def gallery_params
    params.require(:gallery).permit!
  end		

end
class DownloadableFormsController < ApplicationController

  def index
    @downloadable_forms = DownloadableForm.all
    @downloadable_forms = @downloadable_forms.page(params[:page]).per(15)
  end

end
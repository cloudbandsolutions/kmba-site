class PagesController < ApplicationController		
	
	def index
		@event_and_feature = EventsAndFeature.all.order("published_at DESC").first
		@prev_event_and_feature = EventsAndFeature.all.order("published_at DESC").second
		@previ_event_and_feature = EventsAndFeature.all.order("published_at DESC").third
		@membership = Membership.first
	end

	def about_us
		@kmba_board_of_trustees = Organization.where("title = ?", "KMBA BOARD OF TRUSTEES").first
    	@kmba_board_of_advisers = Organization.where("title = ?", "KMBA BOARD OF ADVISERS").first
    	@kmba_coordinators = Organization.where("title = ?", "KMBA COORDINATORS").first
	end

	def history
		@history = History.first
	end
	
	def history_kdci	
	end

	def vision_and_mission
		@vision_and_mission = VisionAndMission.first
	end

	def association_objective
		@association_objective = AssociationObjective.first
	end

	def other_services	
	end
end

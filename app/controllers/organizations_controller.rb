class OrganizationsController < ApplicationController

  def index
    @kmba_board_of_trustees = Organization.where("title = ?", "KMBA BOARD OF TRUSTEES").first
    @kmba_board_of_advisers = Organization.where("title = ?", "KMBA BOARD OF ADVISERS").first
    @kmba_coordinators = Organization.where("title = ?", "KMBA COORDINATORS").first
  end
end
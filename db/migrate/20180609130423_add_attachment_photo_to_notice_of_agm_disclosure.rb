class AddAttachmentPhotoToNoticeOfAgmDisclosure < ActiveRecord::Migration
  def self.up
    change_table :notice_of_agm_disclosures do |t|
      t.attachment :photo
    end
  end

  def self.down
    remove_attachment :notice_of_agm_disclosures, :photo
  end
end

class AddAttachmentFileAndAttachmentPhotoToPublication < ActiveRecord::Migration
  def self.up
    	change_table :publications do |t|
      		t.attachment :file
      		t.attachment :photo
    	end
  	end

  	def self.down
    	remove_attachment :publications, :file
    	remove_attachment :publications, :photo
  	end
end

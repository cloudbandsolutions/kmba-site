class CreateCompanyDocumentFiles < ActiveRecord::Migration
  def change
    create_table :company_document_files do |t|
      t.integer :company_document_id

      t.timestamps null: false
    end
  end
end

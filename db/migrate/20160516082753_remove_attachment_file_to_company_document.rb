class RemoveAttachmentFileToCompanyDocument < ActiveRecord::Migration
  def change
  	remove_attachment :company_documents, :file
  end
end

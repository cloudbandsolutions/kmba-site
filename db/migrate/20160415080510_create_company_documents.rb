class CreateCompanyDocuments < ActiveRecord::Migration
  def change
    create_table :company_documents do |t|
      t.string :title
      t.string :category
      t.text :content
      t.string :year

      t.timestamps null: false
    end
  end
end

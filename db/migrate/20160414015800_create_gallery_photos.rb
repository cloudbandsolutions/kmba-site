class CreateGalleryPhotos < ActiveRecord::Migration
  def change
    create_table :gallery_photos do |t|
      t.integer :gallery_id

      t.timestamps null: false
    end
  end
end

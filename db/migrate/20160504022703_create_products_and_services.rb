class CreateProductsAndServices < ActiveRecord::Migration
  def change
    create_table :products_and_services do |t|
      t.string :title
      t.text :description

      t.timestamps null: false
    end
  end
end

class RenameActiveMemberKdciToActiveMemberKcoop < ActiveRecord::Migration[7.0]
  def change
    rename_column :memberships, :active_member_kdci,:active_member_kcoop
  end
end

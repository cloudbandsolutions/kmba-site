class CreateEventsAndFeatures < ActiveRecord::Migration
  def change
    create_table :events_and_features do |t|
      t.string :title

      t.timestamps null: false
    end
  end
end

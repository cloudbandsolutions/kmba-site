class AddAttachmentPhotoToProductsAndServices < ActiveRecord::Migration
  def self.up
    change_table :products_and_services do |t|
      t.attachment :photo
    end
  end

  def self.down
    remove_attachment :products_and_services, :photo
  end
end

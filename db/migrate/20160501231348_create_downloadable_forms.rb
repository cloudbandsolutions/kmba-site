class CreateDownloadableForms < ActiveRecord::Migration
  def change
    create_table :downloadable_forms do |t|
      t.string :title

      t.timestamps null: false
    end
  end
end

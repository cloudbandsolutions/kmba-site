class CreateContactUs < ActiveRecord::Migration
  def change
    create_table :contact_us do |t|
      t.string :street
      t.string :subdivision
      t.string :barangay
      t.string :country
      t.string :contact
      t.string :email
      t.text :map_link

      t.timestamps null: false
    end
  end
end

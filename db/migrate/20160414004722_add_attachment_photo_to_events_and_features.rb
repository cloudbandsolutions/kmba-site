class AddAttachmentPhotoToEventsAndFeatures < ActiveRecord::Migration
  def self.up
    change_table :events_and_features do |t|
      t.attachment :photo
    end
  end

  def self.down
    remove_attachment :events_and_features, :photo
  end
end

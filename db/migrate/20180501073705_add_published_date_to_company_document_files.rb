class AddPublishedDateToCompanyDocumentFiles < ActiveRecord::Migration
  def change
  	add_column :company_document_files,:published_date,:date
  end
end

class CreateVisionAndMissions < ActiveRecord::Migration[5.0]
  def change
    create_table :vision_and_missions do |t|
      t.text :content

      t.timestamps
    end
  end
end

class AddAttachmentFileToOtherDisclosures < ActiveRecord::Migration
	def self.up
    	change_table :other_disclosures do |t|
      		t.attachment :file
    	end
  	end

  	def self.down
    	remove_attachment :other_disclosures, :file
  	end
end

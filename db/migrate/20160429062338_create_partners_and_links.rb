class CreatePartnersAndLinks < ActiveRecord::Migration
  def change
    create_table :partners_and_links do |t|
      t.string :title
      t.text :content

      t.timestamps null: false
    end
  end
end

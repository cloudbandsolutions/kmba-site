class ModifyYearFieldToCompanyDocument < ActiveRecord::Migration
  def change
  	change_column :company_documents, :year, :integer
  end
end

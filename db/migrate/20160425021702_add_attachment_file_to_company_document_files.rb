class AddAttachmentFileToCompanyDocumentFiles < ActiveRecord::Migration
  def self.up
    change_table :company_document_files do |t|
      t.attachment :file
    end
  end

  def self.down
    remove_attachment :company_document_files, :file
  end
end

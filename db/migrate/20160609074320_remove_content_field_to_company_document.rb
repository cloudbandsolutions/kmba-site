class RemoveContentFieldToCompanyDocument < ActiveRecord::Migration
  def change
  	remove_column :company_documents, :content
  end
end

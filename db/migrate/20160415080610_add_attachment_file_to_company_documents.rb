class AddAttachmentFileToCompanyDocuments < ActiveRecord::Migration
  def self.up
    change_table :company_documents do |t|
      t.attachment :file
    end
  end

  def self.down
    remove_attachment :company_documents, :file
  end
end

class AddClaimSummaryHiipToMemberships < ActiveRecord::Migration[7.0]
  def change
    add_column :memberships, :claims_summary_HIIP, :string
  end
end

class AddAttachmentFileToFiles < ActiveRecord::Migration
  def self.up
    change_table :link_files do |t|
      t.attachment :file
    end
  end

  def self.down
    remove_attachment :link_files, :file
  end
end

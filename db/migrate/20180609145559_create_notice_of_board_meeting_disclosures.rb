class CreateNoticeOfBoardMeetingDisclosures < ActiveRecord::Migration
  def change
    create_table :notice_of_board_meeting_disclosures do |t|
    	t.string :title
    	t.text :description
        t.date :published_at

      t.timestamps null: false
    end
  end
end

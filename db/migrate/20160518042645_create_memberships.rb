class CreateMemberships < ActiveRecord::Migration
  def change
    create_table :memberships do |t|
      t.date :date_updated
      t.string :financial_highlights_assets
      t.string :financial_highlights_liabilities
      t.string :financial_highlights_fund_balance
      t.string :active_member_kdci
      t.string :active_member_associates
      t.string :active_member_total
      t.string :claims_summary_BLIP
      t.string :claims_summary_CLIP
      t.string :claims_summary_total
      t.string :kalinga_number_of_POC_sold
      t.string :kalinga_number_of_claims
      t.string :kalinga_amount_of_claims
      t.string :sicat_number_of_claims
      t.string :sicat_amount_of_claims

      t.timestamps null: false
    end
  end
end

class AddClaimsSummaryKbenteAndClaimsSummaryCalamityAssistanceToMemberships < ActiveRecord::Migration[5.0]
  def change
  	add_column :memberships, :claims_summary_kbente, :string
  	add_column :memberships, :claims_summary_calamity_assistance, :string
  end
end

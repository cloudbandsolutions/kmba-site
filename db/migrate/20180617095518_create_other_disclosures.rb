class CreateOtherDisclosures < ActiveRecord::Migration
  def change
    create_table :other_disclosures do |t|
    	t.string :title
    	t.date :published_date

      t.timestamps null: false
    end
  end
end

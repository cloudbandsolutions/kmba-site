class CreateAssociationObjectives < ActiveRecord::Migration[5.0]
  def change
    create_table :association_objectives do |t|
      t.text :content

      t.timestamps
    end
  end
end
